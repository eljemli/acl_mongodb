package com.test.folder.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "dashboard" )
public class Dashboard {

	private String id;
	private String name;

	@DBRef
	private Folder dashboard;

	public Dashboard() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Folder getDashboard() {
		return dashboard;
	}

	public void setDashboard(Folder dashboard) {
		this.dashboard = dashboard;
	}
	
	
}
