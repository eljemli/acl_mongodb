package com.test.folder.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "acl_object_identity")
public class AclObjectIdentity {

	@Id
	private String id;

	@DBRef
	private AclClass objectIdClass;

	private String objectIdIdentity;

	@DBRef
	private User ownerId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public AclClass getObjectIdClass() {
		return objectIdClass;
	}

	public void setObjectIdClass(AclClass objectIdClass) {
		this.objectIdClass = objectIdClass;
	}

	public String getObjectIdIdentity() {
		return objectIdIdentity;
	}

	public void setObjectIdIdentity(String objectIdIdentity) {
		this.objectIdIdentity = objectIdIdentity;
	}


	public User getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(User ownerId) {
		this.ownerId = ownerId;
	}

}