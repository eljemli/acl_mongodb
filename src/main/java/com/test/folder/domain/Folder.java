package com.test.folder.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "folder" )
public class Folder {

	private String id;
	private String name;

	@DBRef
	private List<AclObjectIdentity> accounts;

	public Folder() {
		super();
	}

	public Folder(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<AclObjectIdentity> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<AclObjectIdentity> accounts) {
		this.accounts = accounts;
	}

}
