package com.test.folder.service;

import com.test.folder.domain.Folder;

public interface FolderService {

	Iterable<Folder> getAllFolder();

}
