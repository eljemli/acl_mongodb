package com.test.folder.service;

import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.folder.domain.AclClass;
import com.test.folder.domain.AclObjectIdentity;
import com.test.folder.domain.User;
import com.test.folder.repositories.AclClassRepository;
import com.test.folder.repositories.AclObjectIdentityRepository;
import com.test.folder.repositories.UserRepositoryDao;

@Service(value="permissionService")
public class PermissionServiceImpl implements PermissionService {

	
	@Autowired
	private  AclObjectIdentityRepository aclObjectDao;
	
	@Autowired
	private  UserRepositoryDao userDao;
	
	@Autowired
	private AclClassRepository aclClassDao;
	
	@Override
	public boolean hadAccess(String className,String EntityId,String authentication) {
		boolean hasPermission = false;
		if(StringUtils.isNotEmpty(EntityId)){
			AclClass folderClass = aclClassDao.findByClassName(className);
			User current = userDao.findByUsername(authentication);
			AclObjectIdentity aclObject	= aclObjectDao.isOwnerOfObject(new ObjectId(folderClass.getId()),  new ObjectId(EntityId), new ObjectId(current.getId()));
			if(aclObject!=null){
				hasPermission =true;
			}
		}
		return hasPermission;
	}

}
