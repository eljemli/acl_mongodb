package com.test.folder.repositories;
import org.springframework.data.repository.CrudRepository;

import com.test.folder.domain.AclClass;


public interface AclClassRepository extends CrudRepository<AclClass, String> {

	AclClass findByClassName(String className);

}
