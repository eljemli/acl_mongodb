package com.test.folder.repositories;

import org.springframework.data.repository.CrudRepository;

import com.test.folder.domain.User;


public interface UserRepositoryDao extends CrudRepository<User, String> {
	
	
	public User findByUsername(String username);

}
